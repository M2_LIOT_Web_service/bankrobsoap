const mongoose = require('mongoose');
//const bcrypt   = require('bcryptjs');

const Portefeuille = new mongoose.Schema({
    _id: { type:String },
    
    id_user: {
        type: String,
        trim: true,
    },
    argent: {
        type: String,
        trim: true,
    }
});
// Group.pre('save', (next) => {
//     // if (!this.isModified('password')) {
//     //     return next();
//     // }
//     //this.password = bcrypt.hashSync(this.password, 10);
//     const id = mongoose.Types.ObjectId();
//     this._id = id;
//     console.log('const id : %s', id);
//     console.log('save _id : %s', this._id);
//     next();
// });
module.exports = mongoose.model('Portefeuille', Portefeuille);