const express = require('express');
const app = express();
const morgan = require('morgan');
const mongodb = require('./db/mongo');
const cors = require('cors');
const soap = require('soap');
const func = require('./fn/function');

mongodb.InitDbConnection();

app.use(cors({
    exposedHeaders: ['Authorization'],
    origin: '*'
}));
app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

var serviceSoap = require('./service/PortefeuilleServiceSoap');

const xml = require('fs').readFileSync('./portefeuille.wsdl', 'utf8');
app.listen(func.normalizePort(process.env.PORT || '8020'), () =>{
    soap.listen(app, '/portefeuille', serviceSoap, xml, ()=>{
        console.log('server portefeuille initialized');
    });
});