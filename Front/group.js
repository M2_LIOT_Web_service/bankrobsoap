const regexDoc = /<_doc>(.+?)<\/_doc>/gim;
const regexID = /<_id>(.*?)<\/_id>/gim;
const regexName = /<name>(.*?)<\/name>/gim;
const regexOwner = /<owner>(.*?)<\/owner>/gim;
const regexIdGroup = /<id_group>(.*?)<\/id_group>/gim;
const regexIdMember = /<id_member>(.*?)<\/id_member>/gim;

function getGroups() {
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.open("POST", "http://localhost:8020/group", true);

  var sr =
  '<?xml version="1.0" encoding="utf-8"?>' + 
  '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">' +
    '<soap:Body>' +
      '<getGroups>' +
      '</getGroups>' +
    '</soap:Body>' +
  '</soap:Envelope>';

  xmlhttp.onreadystatechange = function () {
    if (xmlhttp.readyState == 4) {
      if (xmlhttp.status == 200) {
        console.log(xmlhttp.responseText);
        let responseTxt = xmlhttp.responseText;
        let groups = regexDoc.exec(responseTxt);
        let endLoop = false;
        let contentText = "";
        while(!endLoop ){
            if (groups != null){
                regexID.lastIndex = 0;
                regexName.lastIndex = 0;
                regexOwner.lastIndex = 0;
                contentText += `Group : ${regexName.exec(groups[1])[1]}\r\n`
                + `Owner : ${regexOwner.exec(groups[1])[1]}\r\n`
                + `id : ${regexID.exec(groups[1])[1]}\r\n\r\n`
                responseTxt.replace(groups[0],"");
                groups = regexDoc.exec(responseTxt);
            }else {
                endLoop = true;
            }
        }
        document.getElementById("displaydata").innerHTML = contentText;
      } else {
        console.log(xmlhttp.responseText);
        console.log("error");
          alert(xmlhttp.responseText);
      }
    }
  };
  xmlhttp.setRequestHeader("getGroups", "http://localhost:8020/group");
  xmlhttp.setRequestHeader("Content-Type", "text/xml");
  xmlhttp.send(sr);
}

function getAssociations() {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", "http://localhost:8020/group", true);
  
    var sr =
    '<?xml version="1.0" encoding="utf-8"?>' + 
    '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">' +
      '<soap:Body>' +
        '<getAssociations>' +
        '</getAssociations>' +
      '</soap:Body>' +
    '</soap:Envelope>';
  
    xmlhttp.onreadystatechange = function () {
      if (xmlhttp.readyState == 4) {
        if (xmlhttp.status == 200) {
          console.log(xmlhttp.responseText);
          let responseTxt = xmlhttp.responseText;
          let groups = regexDoc.exec(responseTxt);
          let endLoop = false;
          let contentText = "";
          while(!endLoop ){
              if (groups != null){
                regexIdGroup.lastIndex = 0;
                regexIdMember.lastIndex = 0;
                  contentText += `Id Group : ${regexIdGroup.exec(groups[1])[1]}\r\n`
                  + `Id Member : ${regexIdMember.exec(groups[1])[1]}\r\n\r\n`
                  responseTxt.replace(groups[0],"");
                  groups = regexDoc.exec(responseTxt);
              }else {
                  endLoop = true;
              }
          }
          document.getElementById("displaydata").innerHTML = contentText;
        } else {
          console.log(xmlhttp.responseText);
          console.log("error");
            alert(xmlhttp.responseText);
        }
      }
    };
    xmlhttp.setRequestHeader("getAssociations", "http://localhost:8020/group");
    xmlhttp.setRequestHeader("Content-Type", "text/xml");
    xmlhttp.send(sr);
  }

  function getGroupsByMember() {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", "http://localhost:8020/group", true);
    const id_member = document.getElementById("id_member_entry").value;
    console.log("input id_member : ", id_member);
    var sr =
    '<?xml version="1.0" encoding="utf-8"?>' + 
    '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">' +
      '<soap:Body>' +
        '<getGroupsByMember>' +
            '<id_member>'+ id_member + '</id_member>' +
        '</getGroupsByMember>' +
      '</soap:Body>' +
    '</soap:Envelope>';
  
    xmlhttp.onreadystatechange = function () {
      if (xmlhttp.readyState == 4) {
        if (xmlhttp.status == 200) {
          console.log(xmlhttp.responseText);
          let responseTxt = xmlhttp.responseText;
          let groups = regexDoc.exec(responseTxt);
          let endLoop = false;
          let contentText = "";
          while(!endLoop ){
              if (groups != null){
                  regexID.lastIndex = 0;
                  regexName.lastIndex = 0;
                  regexOwner.lastIndex = 0;
                  contentText += `Group : ${regexName.exec(groups[1])[1]}\r\n`
                  + `Owner : ${regexOwner.exec(groups[1])[1]}\r\n`
                  + `id : ${regexID.exec(groups[1])[1]}\r\n\r\n`
                  responseTxt.replace(groups[0],"");
                  groups = regexDoc.exec(responseTxt);
              }else {
                  endLoop = true;
              }
          }
          document.getElementById("displaydata").innerHTML = contentText;
        } else {
          console.log(xmlhttp.responseText);
          console.log("error");
            alert(xmlhttp.responseText);
        }
      }
    };
    xmlhttp.setRequestHeader("getGroupsByMember", "http://localhost:8020/group");
    xmlhttp.setRequestHeader("Content-Type", "text/xml");
    xmlhttp.send(sr);
  }

  function addGroup(){
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", "http://localhost:8020/group", true);
    const groupName = document.getElementById("group_name").value;
    const groupOwner = document.getElementById("group_owner").value;
    var sr =
    '<?xml version="1.0" encoding="utf-8"?>' + 
    '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">' +
      '<soap:Body>' +
        '<addGroup>' +
            '<group>' +
                '<name>' + groupName +'</name>' +
                '<owner>' + groupOwner + '</owner>' +
            '</group>' +
        '</addGroup>' +
      '</soap:Body>' +
    '</soap:Envelope>';
  
    xmlhttp.onreadystatechange = function () {
      if (xmlhttp.readyState == 4) {
        if (xmlhttp.status == 200) {
          console.log(xmlhttp.responseText);
          let responseTxt = xmlhttp.responseText;
          let contentText = "";

          document.getElementById("displaydata").innerHTML = responseTxt;
        } else {
          console.log(xmlhttp.responseText);
          console.log("error");
            alert(xmlhttp.responseText);
        }
      }
    };
    xmlhttp.setRequestHeader("addGroup", "http://localhost:8020/group");
    xmlhttp.setRequestHeader("Content-Type", "text/xml");
    xmlhttp.send(sr);
  }

  function deleteGroup(){
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", "http://localhost:8020/group", true);
    const idGroup = document.getElementById("id_group").value;
    var sr =
    '<?xml version="1.0" encoding="utf-8"?>' + 
    '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">' +
      '<soap:Body>' +
        '<deleteGroup>' +
            '<id_groupe>' + idGroup + '</id_groupe>' +
        '</deleteGroup>' +
      '</soap:Body>' +
    '</soap:Envelope>';
  
    xmlhttp.onreadystatechange = function () {
      if (xmlhttp.readyState == 4) {
        if (xmlhttp.status == 200) {
          console.log(xmlhttp.responseText);
          let responseTxt = xmlhttp.responseText;
          let contentText = "";

          document.getElementById("displaydata").innerHTML = responseTxt;
        } else {
          console.log(xmlhttp.responseText);
          console.log("error");
            alert(xmlhttp.responseText);
        }
      }
    };
    xmlhttp.setRequestHeader("deleteGroup", "http://localhost:8020/group");
    xmlhttp.setRequestHeader("Content-Type", "text/xml");
    xmlhttp.send(sr);
  }
