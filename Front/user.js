const regexDoc = /<_doc>(.+?)<\/_doc>/gim;

function getUsers() {
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.open("POST", "http://localhost:8000/user", true);
  
    var sr =
    '<?xml version="1.0" encoding="utf-8"?>' + 
    '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">' +
      '<soap:Body>' +
        '<getUsers>' +
        '</getUsers>' +
      '</soap:Body>' +
    '</soap:Envelope>';
  
    xmlhttp.onreadystatechange = function () {
      if (xmlhttp.readyState == 4) {
        if (xmlhttp.status == 200) {
          console.log(xmlhttp.responseText);
          let responseTxt = xmlhttp.responseText;
          let groups = regexDoc.exec(responseTxt);
          let endLoop = false;
          let contentText = "";
          while(!endLoop ){
              if (groups != null){
                  contentText += `Group : \r\n${groups[1]}\r\n\r\n`;
                  responseTxt.replace(groups[0],"");
                  groups = regexDoc.exec(responseTxt);
              }else {
                  endLoop = true;
              }
          }
          document.getElementById("displaydata").innerHTML = contentText;
        } else {
          console.log(xmlhttp.responseText);
          console.log("error");
            alert(xmlhttp.responseText);
        }
      }
    };
    xmlhttp.setRequestHeader("getUsers", "http://localhost:8020/group");
    xmlhttp.setRequestHeader("Content-Type", "text/xml");
    xmlhttp.send(sr);
  }