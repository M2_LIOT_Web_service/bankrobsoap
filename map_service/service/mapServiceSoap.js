const serviceSoap = {
    map_service : {
        map_PortType : {
            getItinerary: function(args) {
                var xStart = args.contactDetailsXY.xStart;
                var yStart = args.contactDetailsXY.yStart;
                var xEnd = args.contactDetailsXY.xEnd;
                var yEnd = args.contactDetailsXY.yEnd;
                
                return { itinerary: "xStart: " + xStart + "; yStart: " + yStart + "; xEnd: " + xEnd + "; yEnd: " + yEnd }
            },
            getItineraryFaster: function(args) {
                return { itinerary: "itinerary faster OK" }
            },
            getItineraryWithoutPlace: function(args) {
                return { itinerary: "itinerary without place OK" }
            },
            getItineraryFasterWithoutPlace: function(args) {
                return { itinerary: "itinerary faster without place OK" }
            },
            getItineraryWithPlace: function(args) {
                return { itinerary: "itinerary with place OK" }
            },
            getItineraryFasterWithPlace: function(args){
                return { itinerary: "itinerary with place OK" }
            }
        }
    }
}

module.exports = serviceSoap;