const notifier = require('node-notifier');
const path = require('path');
// https://www.npmjs.com/package/node-notifier

notifier.notify(
  {
    title: 'My awesome title',
    message: 'Hello from node, Mr. User!',
    icon: path.join(__dirname, 'coulson.jpg'), // Absolute path (doesn't work on balloons)
    sound: true, // Only Notification Center or Windows Toasters
    wait: true // Wait with callback, until user action is taken against notification, does not apply to Windows Toasters as they always wait or notify-send as it does not support the wait option
  },
  function (err, response, metadata) {
    // Response is response from notification
    // Metadata contains activationType, activationAt, deliveredAt
  }
);

notifier.on('click', function (notifierObject, options, event) {
  // Triggers if `wait: true` and user clicks notification
});

notifier.on('timeout', function (notifierObject, options) {
  // Triggers if `wait: true` and notification closes
});

const NotificationCenter = require('node-notifier/notifiers/notificationcenter');
new NotificationCenter(options).notify();

const NotifySend = require('node-notifier/notifiers/notifysend');
new NotifySend(options).notify();

const WindowsToaster = require('node-notifier/notifiers/toaster');
new WindowsToaster(options).notify();

const Growl = require('node-notifier/notifiers/growl');
new Growl(options).notify();

const WindowsBalloon = require('node-notifier/notifiers/balloon');
new WindowsBalloon(options).notify();

const nn = require('node-notifier');

new nn.NotificationCenter(options).notify();
new nn.NotifySend(options).notify();
new nn.WindowsToaster(options).notify(options);
new nn.WindowsBalloon(options).notify(options);
new nn.Growl(options).notify(options);

notifier.notify(
    {
      title: undefined,
      subtitle: undefined,
      message: undefined,
      sound: false, // Case Sensitive string for location of sound file, or use one of macOS' native sounds (see below)
      icon: 'Terminal Icon', // Absolute Path to Triggering Icon
      contentImage: undefined, // Absolute Path to Attached Image (Content Image)
      open: undefined, // URL to open on Click
      wait: false, // Wait for User Action against Notification or times out. Same as timeout = 5 seconds
  
      // New in latest version. See `example/macInput.js` for usage
      timeout: 5, // Takes precedence over wait if both are defined.
      closeLabel: undefined, // String. Label for cancel button
      actions: undefined, // String | Array<String>. Action label or list of labels in case of dropdown
      dropdownLabel: undefined, // String. Label to be used if multiple actions
      reply: false // Boolean. If notification should take input. Value passed as third argument in callback and event emitter.
    },
    function (error, response, metadata) {
      console.log(response, metadata);
    }
  );