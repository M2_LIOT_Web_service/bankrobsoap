const express = require('express');
const app = express();
const morgan = require('morgan');
const cors = require('cors');
const soap = require('soap');
const func = require('./fn/function');

app.use(cors({
    exposedHeaders: ['Authorization'],
    origin: '*'
}));
app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

var serviceSoap = require('./service/notifierServiceSoap');

const xml = require('fs').readFileSync('./notifier.wsdl', 'utf8');
app.listen(func.normalizePort(process.env.PORT || '8022'), () =>{
    soap.listen(app, '/notifier', serviceSoap, xml, ()=>{
        console.log('server notifier initialized');
    });
});