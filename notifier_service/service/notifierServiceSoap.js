var nodemailer = require("nodemailer");
const notifier = require('node-notifier');

var transporter = nodemailer.createTransport({
  service: "gmail",
  host: "smtp.gmail.com",
  // port: 587,
  secure: false,
  requireTLS: true,
  auth: {
    user: "bankrobapi@gmail.com",
    pass: "bankrobynov",
  },
});

const serviceSoap = {
  notifier_service: {
    notifier_port: {
      sendMail: (args, callback) => {
        const from = `<${args.mail.from}>`;
        const to = args.mail.to;
        const subject = args.mail.subject;
        const text = args.mail.text;
        if (!from && !to) {
          return { message: "from & to must be filled" };
        }
        var mailOptions = {
          from: from,
          to: to,
          subject: subject,
          text: text
        };

        transporter.sendMail(mailOptions, function (error, info) {
          if (error) {
            console.log(error);
            callback({ message: error });
          } else {
            console.log("Email sent: " + info.response);
            callback({ message: "Email has been sent" });
          }
        });
      },

      sendNotification: (args, callback) => {
        const title = args.notification.title;
        const message = args.notification.message;
        if(title == "" && message == ""){
          return { message: "not implemented" };
        }
        notifier.notify({title: title, message: message}, function(error, response){
          if(error){
            console.log(error);
            callback({ message: error });
          } else {
            console.log("Notification sent: " + response);
            callback({ message: "Notification has been sent" })
          }
        });
      },
    },
  },
};

module.exports = serviceSoap;
