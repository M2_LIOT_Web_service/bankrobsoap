const Group = require('../models/group');
const Association = require('../models/association');
const mongoose = require('mongoose');

const serviceSoap = {
    group_service : {
        group_port : {
            getGroups: (args, callback) => {
                Group.find()
                .then(groups => callback({groups : groups }))
                .catch(err => callback(err.message));
            },

            getGroupsByMember: (args, callback) => {
                console.log(args.id_member);
                Association.find({ id_member : args.id_member})
                .then(asso => {
                    var groups = [];
                    var request = new Promise((resolve, reject) => {
                        asso.forEach((item, index, array) => {                        
                            Group.findById({_id : item.id_group})
                            .exec()
                            .then(group => {
                                groups.push(group);
                                if(index === array.length - 1) resolve();    
                            })
                            .catch(err => callback(err.message));
                        });
                    });
                    request.then(()=>{
                        callback({groups : groups});
                    });
                })
                .catch(err => callback(err.message));
            },

            getAssociations: (args, callback) => {
                Association.find()
                .then(ass => callback({ associations : ass }))
                .catch(err => callback(err.message));
            },

            addGroup: (args, callback) => {
                const _id = mongoose.Types.ObjectId();
                const newGroup = new Group({...args.group, _id});
                newGroup.save((err, newGroup) => {
                    if(err){
                        callback(err.message);
                    } else {                        
                        callback({ group : newGroup });
                    } 
                });
            },

            associateGroup: (args, callback) => {
                const newAssociation = new Association(args.association);
                newAssociation.save((err)=>{
                    if(err){
                        callback({ message : err.message });
                    } else {
                        callback({ message : 'Le membre est associé au groupe' });
                    }
                });
            },

            dissociateGroup: (args, callback) => {
                Association.findOneAndDelete(args.association)
                .then(result => callback({ message : 'L\'association est supprimée' }))
                .catch(err => callback({ message : err.message }));
            },

            deleteGroup: (args, callback) => {
                Group.findOneAndDelete({_id: args.id_groupe})
                .then(res => callback({message: res}))
                .catch(err => callback({message: err}));
            }
        }
    }
}

module.exports = serviceSoap;