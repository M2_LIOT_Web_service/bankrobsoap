const mongoose = require('mongoose');
//const bcrypt   = require('bcryptjs');

const Group = new mongoose.Schema({
    _id: { type:String },
    name: {
        type: String,
        trim: true,
        required: [true, 'Le nom est obligatoire']
    },
    owner: {
        type: String,
        trim: true,
        required: [true, 'Le chef de groupe est obligatoire']
    }
});
// Group.pre('save', (next) => {
//     // if (!this.isModified('password')) {
//     //     return next();
//     // }
//     //this.password = bcrypt.hashSync(this.password, 10);
//     const id = mongoose.Types.ObjectId();
//     this._id = id;
//     console.log('const id : %s', id);
//     console.log('save _id : %s', this._id);
//     next();
// });
module.exports = mongoose.model('Group', Group);